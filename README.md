# Ejercicios del curso Introducción a la programación

Este repositorio almancena las soluciones a los ejercicios planteados en el curso de fundamentos de programación.

## Autor: 
Ramírez Fuentes Edgar Alejandro - [EdgarRamirezFuentes](https://github.com/EdgarRamirezFuentes)

### Contacto
edgar.ramirez.fuentes.dev@gmail.com


## Instructor
Barajas González Luis Daniel - [danielbg.dev](https://gitlab.com/danielbg.dev)