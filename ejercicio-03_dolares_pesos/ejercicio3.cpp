/**
 * Ejercicio 3: Dólares a pesos
 * Problema: Elabora un programa que reciba como entrada un monto en dólares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
 * Nombre: ejercicio3.cpp
 * Autor: Ramírez Fuentes Edgar Alejandro
 * Fecha de última modificación: 11/10/2021
 */

#include <iostream>
#include <iomanip>

double convertir_dolares_pesos(double, double);

int main(void) {
    double monto_dolares = 0.00;
    // Almacena el valor que tiene un dólar (USD) en pesos mexicanos
    double valor_pesos_mx = 20.88;
    do {
        std::cin >> monto_dolares;
        if (monto_dolares < 0) { std::cout << "Ingresa un monto de dolares valido\n"; }
    } while (monto_dolares < 0);

    // Brinda formato para que solo se muestren dos decimales
    std::cout.setf(std::ios::fixed);
    std::cout.precision(2);

    std::cout << convertir_dolares_pesos(monto_dolares, valor_pesos_mx) << "\n";
    return 0;
}

/**
 * Hace la conversión de dólares (USD) a pesos
 * @param monto_dolares es el monto de dólares (USD) que se va a convertir
 * @param valor_pesos_mx es el valor actual que tiene 1 dólar en pesos mexicanos
 * 
 * @return el monto de pesos mexicanos que equivalen al monto de dólares (USD) recibido
 */ 
double convertir_dolares_pesos(double monto_dolares, double valor_pesos_mx) {
    return monto_dolares * valor_pesos_mx;
}

