#include <iostream>

double calcular_perimetro(double, double, double);

int main() {
    double lado_1 = 0, lado_2 = 0, lado_3 = 0;
    do {
        std::cout << "Ingresa el valor de lado 1: ";
        std::cin >> lado_1;
        std::cout << "Ingresa el valor de lado 2: ";
        std::cin >> lado_2;
        std::cout << "Ingresa el valor de lado 3: ";
        std::cin >> lado_3;
        if (lado_1 <= 0 || lado_2 <= 0 || lado_3 <= 0 || lado_1 == lado_2 || lado_2 == lado_3 || lado_1 == lado_3) {
            std::cout << "Las medidas de los lados deben ser diferentes y mayores a cero." << std::endl;
        }
    } while (lado_1 <= 0 || lado_2 <= 0 || lado_3 <= 0 || lado_1 == lado_2 || lado_2 == lado_3 || lado_1 == lado_3);
    std::cout << "El perimetro del triangulo es: " << calcular_perimetro(lado_1, lado_2, lado_3) << std::endl;
    return 0;    
}
/**
 * Calcula el perimetro de un triangulo escaleno
 * 
 * @param lado_1 es la medida de uno de los lados del triangulo escaleno
 * @param lado_2 es la medida de uno de los lados del triangulo escaleno
 * @param lado_3 es la medida de uno de los lados del triangulo escaleno
 * @return el parámetro del triangulo escaleno
 */ 
double calcular_perimetro(double lado_1, double lado_2, double lado_3) {
    return lado_1 + lado_2 + lado_3;
}