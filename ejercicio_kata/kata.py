'''
    Ejercicio Kata: Números arábigos y números romanos
    Autor: Ramírez Fuentes Edgar Alejandro
    Nombre: kata.py
    Lenguaje de programación: Python
    Versión: 3.9.6
    Fecha de última modificación: 11/10/2021
    Problema:
    Se tiene un par formado por “AR”
    Donde:
        “A” es un número en sistema decimal
        “R” es un número romano
    El valor del par se calcula multiplicando el valor decimal de “R” por “A”
    Ejemplo:
    5V = (5 * 5) = 25
    La entrada del programa es una cadena formada por dos pares “AR“, sus valores se calculan de la siguiente manera:
    
    a) Se suman los valores de los pares si el "R" del primer par, es mayor o igual al "R" del segundo par.
        3C4X = (3*100) + (4*10)
        3C4X = 300 + 40
        3C4X = 340
    
    b) Se restan los pares si el "R" del primer par es menor al del segundo par
        4X3C = -(4 * 10) - (3 * 100)
        4X3C = -40 + 300
        4X3C = 260
    
    Nota: En esta solución se considera que 
        R solo toma los valores I,V,X,L,C y M  
        A solo toma los valores 0,1,2,3,4,5,6,7,8 y 9.
'''
import re

def es_cadena_valida (cadena : str) -> bool:
    '''
        Evalúa que la cadena cumpla los requerimientos necesarios

        Parameters
        ------------
        cadena : str
            Es la cadena que será evaluada
        
        Returns
        -----------
        bool 
            Es el valor que determina si la cadena es válida o no
    '''
    # Expresion regular que será utilizada para evaluar que la cadena ingresada tiene la forma ARAR
    cadena_regex = r"^(\d[IVXLCDM]){2}$"
    return True  if re.match(cadena_regex, cadena) else False

def obtener_valor_entero (numero_romano : str) -> int:
    '''
        Retorna el valor en el sistema decimal correspondiente al digito romano recibido

        Parameters
        ------------
        numero_romano : str
            Es el número romano del cual se quiere obtener su valor en el sistema decimal

        Returns
        -----------
        int
            Es el valor en el sistema decimal del número romano recibido
    '''
    romano_a_decimal = {
        'I' : 1,
        'V' : 5,
        'X' : 10,
        'L' : 50,
        'C' : 100,
        'D' : 500,
        'M' : 1000
    }
    return romano_a_decimal[numero_romano]

def calcular_valor(cadena : str) -> int:
    '''
        Calcula el valor de la cadena ingresada

        Parameters
        ---------
        cadena : str
            Es la cadena de la cual se va a calcular su valor
        
        Returns
        ----------
        int 
            Es el valor correspondiente a la cadena ingresada
    '''
    # Primer par AR
    A1 = int(cadena[0])
    R1 = obtener_valor_entero(cadena[1])
    #Segundo par AR
    A2 = int(cadena[2])
    R2 = obtener_valor_entero(cadena[3])

    # Únicamente se modifica el valor del primer par dado que la segunda operación siempre se terminará sumando.
    return ((A1 * R1) if R1 >= R2 else (A1 * R1 * -1)) + (A2 * R2)

# Área de pruebas
if __name__ == "__main__":
    pares = ["5V6I", "4X3C", "3C4X", "1A5V", "VV55"]
    for cadena in pares:
        if es_cadena_valida(cadena):
            print(f"El valor de {cadena} es {calcular_valor(cadena)}.")
        else:
            print(f"{cadena} no es una cadena valida.")

