/**
 * Ejercicio 1: Números pares
 * Problema: Elabora un programa que imprima los números pares del 0 al 100.
 * Nombre: ejercicio.cpp
 * Autor: Ramírez Fuentes Edgar Alejandro
 * Fecha de última modificación: 11/10/2021
 */

#include <iostream>

int main(void) {
    // Ciclo que nos ayuda a iterar desde 0 hasta 100 aumentando de 2 en 2
    for (int i = 0; i <= 100; i += 2) {
        std::cout << i << " ";
    }
    return 0;
}