/**
 * Ejercicio 1: Números pares
 * Problema: Elabora un programa que imprima los números pares del 0 al 100.
 * Nombre: ejercicio.cpp
 * Autor: Ramírez Fuentes Edgar Alejandro
 * Fecha de última modificación: 11/10/2021
 */

#include <iostream>

int main(void) {
    // Ciclo que nos ayuda a iterar desde 0 hasta 100
    for (int i = 0; i <= 100; i++) {
        /**
         * El bit más a la derecha de los números pares está apagado
         * 0 -> 0
         * 2 -> 10
         * 4 -> 100
         * 6 -> 110
         * 
         * Entonces al hacer la operación AND de ese número con 1,
         * nos resultará 0 si y solo si el número es par-
         * Ejemplo:
         * 10            11
         * 01            01
         * ___           ---
         * 00            01
         * 
         */

        // Imprime solo aquellos números que aplicando AND con 1 resulta en 0.
        if (!(i & 1)) { std::cout << i << " "; }
    }
    return 0;
}