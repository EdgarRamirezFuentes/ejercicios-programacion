#include <iostream>

int main() {
    double lados_iguales = 0;
    double lado_diferente = 0;
    std::cout << "Ingresa la medida de los lados que tienen la misma medida ";
    do {
        std::cin >> lados_iguales;
        if (lados_iguales <= 0) {
            std::cout << "La medida debe ser mayor a cero" << std::endl;
        }
    } while(lados_iguales <= 0);
    do {
        std::cout << "Ingresa la medida de el lado que tiene medida diferente: ";
        std::cin >> lado_diferente;
        if  (lado_diferente <= 0 || lado_diferente == lados_iguales) {
            std::cout << "La medida debe ser mayor a 0 y diferente a los otros dos lados" << std::endl;
        }   
    } while (lado_diferente <= 0 || lado_diferente == lados_iguales);
    std::cout << "El perimetro del triangulo es: " << calcular_perimetro(lados_iguales, lado_diferente) << std::endl;
    return 0;   
}

/**
 * Calcula el perímetro de un triangulo isosceles
 * 
 * @param lados_iguales es la medida que tienen aquellos lados que tienen la misma medida
 * @param lado_diferente es la medida del lado que tiene diferente medida
 * @return el perímetro de un triangulo isosceles
 */ 
double calcular_perimetro (double lados_iguales, double lado_diferente) {
    return (lados_iguales * 2) + lado_diferente;
}