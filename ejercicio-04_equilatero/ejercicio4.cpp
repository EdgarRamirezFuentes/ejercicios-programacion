#include <iostream>

double calcular_perimetro(double);

int main() {
    double medida_lados = 0.00;
    do {
        std::cout << "Ingresa la medida de los lados del triangulo equilatero: ";
        std::cin >> medida_lados;
        if (medida_lados <= 0){
            std::cout << "La medida de los lados debe ser mayor a 0" << std::endl;
        }
    } while (medida_lados <= 0);
    std::cout << "El périmetro del triangulo equilatero es: " << calcular_perimetro(medida_lados) << std::endl;
    return 0;
}

/**
 * Calcula el perímetro de un triágulo equilatero
 * 
 * @param medida_lados es la medida que tiene cada lado del triángulo equilatero
 * @return el perímetro del triángulo equilatero 
 */ 
double calcular_perimetro(double medida_lados) {
    return medida_lados * 3;
}
