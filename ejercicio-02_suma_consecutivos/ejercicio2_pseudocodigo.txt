numero es un valor entero positivo

numero <- 0
mientras numero <= 0 o numero > 50 hacer
    leer numero
    si numero <= 0 o numero > 50 hacer
        imprimir "Ingresa un entero entre 1 y 50\n"
    fin si
fin mientras

imprimir (numero * (numero + 1)) / 2