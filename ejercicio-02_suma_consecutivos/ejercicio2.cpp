/**
 * Ejercicio 2: Suma de consecutivos
 * Problema: Elabora un programa que reciba un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.
 * Nombre: ejercicio2.cpp
 * Autor: Ramírez Fuentes Edgar Alejandro
 * Fecha de última modificación: 11/10/2021
 */

#include <iostream>

int main(void) {
    int numero = 0;
    // No va a pasar de este paso si el numero no está entre 1 y 50
    while (numero <= 0 || numero > 50) {
        std::cin >> numero;
        if (numero <= 0 || numero > 50) { std::cout << "Ingresa un entero entre 1 y 50\n"; }
    }

    // Utilizando la suma de Gauss podemos obtener el valor de la suma de los números consecutivos.
    std::cout << (numero * (numero + 1)) / 2;
    return 0;
}